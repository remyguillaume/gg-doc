---
sidebar_position: 1
---

# Contributing

There are many ways you can contribute to the geogirafe project.

# Become member of the GeoMapFish Community

https://geomapfish.org/community/

# Financing

You can help us by financing development and maintenance effort.

If you are interested in cofinancing please get in touch with us contact@geomapfish.org.

# Developing

You can get in touch with us if you want to propose contributions to the project or if you want to 
propose your development capabilities for the project. 

# Helping fix issues

You can raise issues and contribute to fix these for the greater good of this open source project.
