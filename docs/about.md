---
sidebar_position: 1
---

# About GeoGirafe

GeoGirafe is an flexible application to build online geoportals.  
A demo instance of GeoGirafe can be tested here: [https://demo.geomapfish.dev/](https://demo.geomapfish.dev/).

GeoGirafe is an open source project supported by the GeoMapFish community. [https://geomapfish.org](https://geomapfish.org)
